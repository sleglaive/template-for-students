from setuptools import setup


with open('README.md') as file:
    long_description = file.read()


# TODO - replace with details of your project
setup(
    name='mypackage',
    description='DESCRIPTION',
    version='0.0.1',
    author='AUTHOR',
    author_email='EMAIL',
    url='https://github.com/USERNAME/NAME',
    install_requires=['pytorch-lightning'],
    packages=['mypackage'],
    package_data={},
    long_description=long_description,
    long_description_content_type='text/markdown',
    keywords=[],
    classifiers=['License :: OSI Approved :: MIT License'],
    license='MIT')
