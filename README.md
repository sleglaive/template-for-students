# Project template

Throughout this template, `mypackage` is used to refer to the name of the project.


## Installation

Clone this repo. Change all instances of `mypackage` to your project name.
Then run `cd mypackage && pip install -e .`.

